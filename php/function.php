<?php
/**
 * 显示错误
 * @param unknown $msg
 */
function showerr($msg, $data){
	global $Db;
	if($Db){
		$Db->close();
	}
	ajaxReturn(array('error'=>1,'info'=>$msg?:'未知错误','data'=>$data,'a'=>$_REQUEST['a']));
}
/**
 * 显示成功
 */
function showsuc($msg,$data=''){
	global $Db;
	if($Db){
		$Db->close();
	}
	ajaxReturn(array('error'=>0,'info'=>$msg?:'操作成功','data'=>$data,'a'=>$_REQUEST['a']));
}

/**
 * 显示成功
 */
function showSucBase($msg,$data=''){
	global $Db;
	if($Db){
		$Db->close();
	}
	ajaxReturn(array('error'=>0,'info'=>$msg?:'操作成功','base'=>$data,'a'=>$_REQUEST['a']));
}

/**
 * ajax返回数据,返回json数据，并终止程序.汉字不编码/u
 */
function ajaxReturn($data){
	header('Content-Type:application/json; charset=utf-8');
	echo json_encode($data);
	exit();
}

/**
 * 测试用，打印数据
 */
function printp($data,$flag = 1){
	echo "<pre>";
	print_r($data);
	echo "</pre>";
	$flag ? die() : '';
}

////////////////////////安全过滤////////////////////////////////
if($_REQUEST){
	foreach ($_REQUEST as $k=>$v){
		$_REQUEST[$k]=strip_tags($v);
		$keyword = 'select|insert|update|delete|union|into|load_file|outfile|sleep| or ';
		$arr = explode( '|', $keyword );
		$v = str_ireplace( $arr, '', $v );
		// $v=str_replace("'", '‘', $v);
		// $v=str_replace('"', '“', $v);
		// $v=str_replace('/', '', $v);
		$_REQUEST[$k]=strip_tags($v);
	}
}
////////////////////////过滤结束////////////////////////////////

// 过滤昵称中的非法字符
function removeEmoji($nickname) {

    $clean_text = $nickname;

    // Match Emoticons
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clean_text = preg_replace($regexEmoticons, '', $clean_text);

    // Match Miscellaneous Symbols and Pictographs
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clean_text = preg_replace($regexSymbols, '', $clean_text);

    // Match Transport And Map Symbols
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clean_text = preg_replace($regexTransport, '', $clean_text);

    // Match Miscellaneous Symbols
    $regexMisc = '/[\x{2600}-\x{26FF}]/u';
    $clean_text = preg_replace($regexMisc, '', $clean_text);

    // Match Dingbats
    $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
    $clean_text = preg_replace($regexDingbats, '', $clean_text);

    // 庆祝符
    $clean_text = preg_replace('/\xEE[\x80-\xBF][\x80-\xBF]|\xEF[\x81-\x83][\x80-\xBF]/', '', $clean_text);

    $clean_text = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]|\xED[\xA0-\xBF][\x80-\xBF]/', '', $clean_text);
    //$return = json_decode(preg_replace("#(\\\ud[0-9a-f]{3})#ie","",json_encode($clean_text)));

    return $clean_text;
}

/**
 * 防止修改数据
 *
 * @param mid       加密的md5字符串 - md5($value . $tid . 'vy888hy')
 * @param tid       时间戳
 * @param value     防修改的值(如：分数，或者其它值)
 */
function verifyMd5($mid, $tid, $value)
{
    if($mid != md5($value . $tid . 'vy888hy')){
        return false;
    }

    return true;
}

// POST CURL 使用
function curlPost($url, $data)
{
    $ch = curl_init();
           
    //设置变量
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//执行结果是否被返回，0是返回，1是不返回
    curl_setopt($ch, CURLOPT_HEADER, 0);//参数设置，是否显示头部信息，1为显示，0为不显示
    //伪造网页来源地址,伪造来自百度的表单提交
    curl_setopt($ch, CURLOPT_REFERER, "http://www.baidu.com");
    //表单数据，是正规的表单设置值为非0
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);//设置curl执行超时时间最大是多少
    //使用数组提供post数据时，CURL组件大概是为了兼容@filename这种上传文件的写法，
    //默认把content_type设为了multipart/form-data。虽然对于大多数web服务器并
    //没有影响，但是还是有少部分服务器不兼容。本文得出的结论是，在没有需要上传文件的
    //情况下，尽量对post提交的数据进行http_build_query，然后发送出去，能实现更好的兼容性，更小的请求数据包。
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    //执行并获取结果
    $output = curl_exec($ch);
    $err = curl_error($ch);
    
    // 释放cURL句柄
    curl_close($ch);
    
    if($output === false)
    {
        return ['errcode'=>1, 'errmsg'=>$err];
    }
    
    return $output;
}

// 微信公众号发送模板消息
function wxSentTemplateMsg($data)
{
    
}

/**
 * 验证手机号是否正确
 *
 * @param number $mobile
 * @return bool
 */
function isMobile($mobile) {
    return preg_match("/^1[34578]{1}\d{9}$/", $mobile) ? true : false;
}

/**
 * 获取毫秒时间戳
 * @return number
 */
function getMillisecond() {
    list($t1, $t2) = explode(' ', microtime());
    return (float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);
}