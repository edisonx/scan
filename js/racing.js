/*


/!* ############## 游戏界面JS代码 ################# *!/

          /!* #### 方向盘 #### *!/
let is_num = 0; //第一次 绝对值
let rotate_deg = 0; //现角度
let temp_rotate = 0; //角度差
let score = 0;
// let btn = document.querySelector(".start"); //获取页面 元素对象
/!*btn.onclick = function () {
    window.addEventListener("deviceorientation", orientationHandler, false);
    time_dowm();
    cumulative();
    document.querySelector('.container_paly').removeChild(btn);
};*!/

window.addEventListener("deviceorientation", orientationHandler, false);
time_dowm();
cumulative();
// document.querySelector('.container_paly').removeChild(btn);

          /!* #### 控制方向盘偏转 #### *!/
function orientationHandler(event) {
    //判断初始值
    if (is_num < 1) {
        rotate_deg = event.beta; //初始角度
        is_num++;
    } else {
        temp_rotate = event.beta - rotate_deg;
        temp_rotate = parseInt(temp_rotate);
        if (temp_rotate) {
            temp_rotate > 60 ? temp_rotate = 60 : temp_rotate;
        } else {
            temp_rotate < -60 ? temp_rotate = -60 : temp_rotate;
        }
    }
    document.querySelector(".wheel").style.transform = "rotate(" + temp_rotate + "deg)  scale(1.0)";
    document.querySelector('.angle').innerHTML = temp_rotate;
    console.log('temp_rotate')
}

          /!* ####  计时器 ####  *!/
function time_dowm() {
    let max = 30;
    let s = 0; //秒数
    let ms = 0; //毫秒 转化成60
    let timer = setInterval(() => {
        if (s < max) {
            if (ms < 60) {
                ms++;
            } else {
                s++;
                ms = 0;
            }
            //更新视图
            if (ms >= 10) {
                document.querySelector('.time_ms').innerHTML = ms;
            } else {
                document.querySelector('.time_ms').innerHTML = '0' + ms;
            }
            if (s < 10) {
                document.querySelector('.time').innerHTML = '0' + s;
            } else {
                document.querySelector('.time').innerHTML = s;
            }
        } else {
            clearInterval(timer);
            alert("Game Over!正在前往领奖台！");
            window.location.href = "end.html"
            /!* 跳转到结算界面 *!/
        }
    }, 16.67);
}

          /!* #### 记分器 ####  *!/
function cumulative() {
    let is_num = 0;
    /!*
    *   1.判断时间
    *   2.   第一次加分。后面都不加
    *   2.  更新视图  ，返回到html
    * *!/
    let time_num = 0;
    let times = [
        5,
        7,
        8,
        10,
        13,
        15,
        16,
        17,
        19,
        20,
        22,
        25,
        27,
        28,
        30
    ];

    let timer = setInterval(function () {
        time_num++;
        // console.log(`time${time_num}`);


        /!*第1次判定并且清零 右转*!/

        console.log('第1次偏转角 + "temp_rotate"');
        if (time_num >= times[0] && time_num < times[1]) {
            if (!is_num) {
                is_num = 1;
                if (temp_rotate > 0) {
                    score += 10;
                    document.getElementById('score').innerHTML = score;

                }
                else if (temp_rotate < 0) {

                    document.getElementById('score').innerHTML = score;
                    document.querySelector('.fail_warning').style.display = "inline";
                }
                else {
                    document.getElementById('score').innerHTML = score;
                }

            }
        }
        if (time_num >= times[1] && time_num < times[2]) {
            is_num = 0;
            console.log('还原');
        }


        /!*第2次判定并且清零 左转*!/
        // console.log('第2次偏转角 + "temp_rotate"');
        if (time_num >= times[2] && time_num < times[3]) {
            if (!is_num) {
                is_num = 1;
                if (temp_rotate < 0) {
                    score += 10;
                    document.getElementById('score').innerHTML = score;

                }
                else if (temp_rotate > 0) {

                    document.getElementById('score').innerHTML = score;
                    document.querySelector('.fail_warning').style.display = "inline";
                }
                else {
                    document.getElementById('score').innerHTML = score;
                }
            }
        }
        if (time_num >= times[3] && time_num < times[4]) {
            is_num = 0;
            console.log('还原');
        }


        /!*第3次判定并且清零 右转*!/
        if (time_num >= times[4] && time_num < times[5]) {
            if (!is_num) {
                is_num = 1;
                if (temp_rotate > 0) {
                    score += 10;
                    document.getElementById('score').innerHTML = score;

                }
                else if (temp_rotate < 0) {

                    document.getElementById('score').innerHTML = score;
                    document.querySelector('.fail_warning').style.display = "inline";
                }
                else {
                    document.getElementById('score').innerHTML = score;
                }
            }
        }
        if (time_num >= times[5] && time_num < times[6]) {
            is_num = 0;
            console.log('还原');
        }


        /!*第4次判定并且清零 右转*!/
        if (time_num >= times[6] && time_num < times[7]) {
            if (!is_num) {
                is_num = 1;
                if (temp_rotate > 0) {
                    score += 10;
                    document.getElementById('score').innerHTML = score;

                }
                else if (temp_rotate < 0) {

                    document.getElementById('score').innerHTML = score;
                    document.querySelector('.fail_warning').style.display = "inline";
                }
                else {
                    document.getElementById('score').innerHTML = score;
                }
            }
        }
        if (time_num >= times[7] && time_num < times[8]) {
            is_num = 0;
            console.log('还原');
        }


        /!*第5次判定并且清零 左转*!/
        if (time_num >= times[8] && time_num < times[9]) {
            if (!is_num) {
                is_num = 1;
                if (temp_rotate < 0) {
                    score += 10;
                    document.getElementById('score').innerHTML = score;

                }
                else if (temp_rotate > 0) {

                    document.getElementById('score').innerHTML = score;
                    document.querySelector('.fail_warning').style.display = "inline";
                }
                else {
                    document.getElementById('score').innerHTML = score;
                }
            }
        }
        if (time_num >= times[9] && time_num < times[10]) {
            is_num = 0;
            console.log('还原');
        }


        /!*第6次判定并且清零 左转*!/
        if (time_num >= times[9] && time_num < times[10]) {
            if (!is_num) {
                is_num = 1;
                if (temp_rotate < 0) {
                    score += 10;
                    document.getElementById('score').innerHTML = score;

                }
                else if (temp_rotate > 0) {

                    document.getElementById('score').innerHTML = score;
                    document.querySelector('.fail_warning').style.display = "inline";
                }
                else {
                    document.getElementById('score').innerHTML = score;
                }
            }
        }
        if (time_num >= times[10] && time_num < times[11]) {
            is_num = 0;
            console.log('还原');
        }


        /!*第7次判定并且清零 右转*!/
        if (time_num >= times[11] && time_num < times[12]) {
            if (!is_num) {
                is_num = 1;
                if (temp_rotate > 0) {
                    score += 10;
                    document.getElementById('score').innerHTML = score;

                }
                else if (temp_rotate < 0) {

                    document.getElementById('score').innerHTML = score;
                    document.querySelector('.fail_warning').style.display = "inline";
                }
                else {
                    document.getElementById('score').innerHTML = score;
                }
            }
        }
        if (time_num >= times[12] && time_num < times[13]) {
            is_num = 0;
            console.log('还原');
        }


        /!*第8次判定并且清零 右转*!/
        if (time_num >= times[13] && time_num < times[14]) {
            if (!is_num) {
                is_num = 1;
                if (temp_rotate > 0) {
                    score += 10;
                    document.getElementById('score').innerHTML = score;

                }
                else if (temp_rotate < 0) {

                    document.getElementById('score').innerHTML = score;
                    document.querySelector('.fail_warning').style.display = "inline";
                }
                else {
                    document.getElementById('score').innerHTML = score;
                }
            }
        }
        if (time_num >= times[14] && time_num < times[15]) {
            is_num = 0;
            console.log('还原');
        }

    }, 1000);
}


/!* ############## 游戏结算界面  ######################## *!/


           /!* #### 获取分数JS #### *!/*/
