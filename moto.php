﻿<?php
    //ini_set("session.save_handler", "memcache");
    //ini_set("session.save_path", "tcp://d02f7ef2dca84c46.m.cnszalist3pub001.ocs.aliyuncs.com:11211");
    error_reporting(E_ERROR);
    date_default_timezone_set('Asia/Chongqing');
    include './php/WeixinAction.php';
    include './php/database.class.php';
    include './php/function.php';
    $config = (require './php/config.php');
    session_start();
    if (empty($_SESSION['_GET'])) {
        $_SESSION['_GET'] = $_GET;
        //所有url的参数都放入session里面
    }
    $Db = new database($config);
    $weixin = new WeixinAction($config, $Db);
    
    ##获取token
    $weixin->_getAccessToken();
    
    if ($_SESSION['openid']) {
        $sql = "select * from userinfo where openid='" . $_SESSION['openid'] . "'";
        $userinfo = $Db->getAll($sql);
    }
    if (!$_SESSION['openid'] || empty($userinfo[0])) {
        // openid不存在或者找不到相关用户记录
        if (!$_REQUEST['code']) {
            $weixin->_getWeixinCode();
        } else {
            $code = $_REQUEST['code'];
            $user_info = $weixin->_getWeixinUserInfo($code);
            if ($user_info['openid']) {
                // 将openid保存到session中
                $_SESSION['openid'] = $user_info['openid'];
                $sql = "select * from userinfo where openid='" . $user_info['openid'] . "'";
                $users = $Db->getAll($sql);
                $nickname = removeEmoji($user_info['nickname']);
                // 昵称
                $openid = $user_info['openid'];
                $headimgurl = $user_info['headimgurl'];
                // 头像地址
                $sex = $user_info['sex'];
                // 性别
                $province = $user_info['province'];
                // 用户个人资料填写的省份
                $city = $user_info['city'];
                // 普通用户个人资料填写的城市
                $country = $user_info['country'];
                // 国家，如中国为CN
                if ($users) {
                    $id = $users[0]['id'];
                    $sql = "update userinfo set nickname='{$nickname}',openid='{$openid}',headimgurl='{$headimgurl}',sex='{$sex}',province='{$province}',city='{$city}',country='{$country}' where id='{$id}'";
                    $Db->execute($sql);
                } else {
                    $sql = "insert into userinfo(nickname,openid,headimgurl,sex,province,city,country) values('{$nickname}','{$openid}','{$headimgurl}','{$sex}','{$province}','{$city}','{$country}')";
                    $Db->execute($sql);
                }
                $_SESSION['userinfo'] = $user_info;
            } elseif ($_SESSION['openid']) {
                $sql = "select * from userinfo where openid='" . $_SESSION['openid'] . "'";
                $users = $Db->getAll($sql);
                if ($users) {
                } else {
                    $sql = "insert into userinfo(openid) values('{$_SESSION['openid']}')";
                    $Db->execute($sql);
                }
            }
        }
    } else {
        $_SESSION['userinfo'] = $userinfo[0];
    }
    $name = $userinfo[0]['nickname'] ? $userinfo[0]['nickname'] : $nickname;
    $nickname = $userinfo[0]['nickname'] ? $userinfo[0]['nickname'] : $nickname;
    $openid = $userinfo[0]['openid'] ?: $_SESSION['openid'];
    $sex = $userinfo[0]['sex'] ?: $sex;
    $city = $userinfo[0]['city'] ?: $city;
    $country = $userinfo[0]['country'] ?: $country;
    $imgURL = $userinfo[0]['headimgurl'] ?: $headimgurl;
    
    $access_token = $_SESSION['WX_ACCESS_TOKEN'];
    echo "<script>window.localStorage.setItem('access_token','{$access_token}');</script>";
    echo "<script>window.localStorage.setItem('nickname','{$nickname}');</script>";
    echo "<script>window.localStorage.setItem('name','{$name}');</script>";
    echo "<script>window.localStorage.setItem('sex','{$sex}');</script>";
    echo "<script>window.localStorage.setItem('city','{$city}');</script>";
    echo "<script>window.localStorage.setItem('country','{$country}');</script>";
    echo "<script>window.localStorage.setItem('openid','{$openid}');</script>";
    echo "<script>window.localStorage.setItem('imgURL','{$imgURL}');</script>";
    //所有get参数都放如localStorage 里面，方便前段调用
    foreach ($_SESSION['_GET'] as $k => $v) {
        echo "<script>window.localStorage.setItem('{$k}','{$v}');</script>";
        setcookie($k, $v);
    }
    if (!empty($_SESSION['_GET']['help_id'])) {
        $help_id = $_SESSION['_GET']['help_id'];
        $nick_toname = $_SESSION['_GET']['nickname'];
        unset($_SESSION['_GET']);
        //echo "<script>window.location='personal.html?id=$help_id&nickname=$nick_toname'</script>";
    }
    unset($_SESSION['_GET']);
    ##根据链接分配公众号授权
    $Db->close();
    // echo "<script>window.location='listen.html'</script>";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>听终极预告片参与「神偷奶爸3」互动</title>
    <link rel="stylesheet" href="css/bass.css">
    <link rel="stylesheet" href="css/common.css">
    <link rel="stylesheet" href="css/listen.css">
    <script src="js/jquery-2.1.3.min.js"></script>
    <!--<script src="js/common.js"></script>-->
    <script src="js/common.js"></script>
    <!--相机扫描图片-->
    <div class="mask">
        <div class="he"></div>
        <p>正在识别</p>
    </div>
</head>

<body>
    <img src="images/stop.png" alt="" style="display:none;">
    <div class="container">
        <div class="backdrop">
            <div id="preloader_1">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        
        <div class="start_btn"></div>
        <!--开始按钮-->

        <!--结束按钮-->
        <div class="btn2"></div>
        <!--收听失败-->
        <div class="listen_fall">
            <span>识别失败</span>
            <span>我们未能识别您收听到的声音，</span>
            <span>可能是由于噪音导致，</span>
            <span>或是您收听了错误的预告片。</span>
            <span>请重新收听。</span>

        </div>

        <!--&lt;!&ndash;底部后加小黄人  左边&ndash;&gt;-->
        <!--<img src="images/yellowleft.png" alt="" class="yellow_manLeft">-->
        <!--&lt;!&ndash;右边&ndash;&gt;-->
        <!--<img src="images/yellowright.png" alt="" class="yellow_manRight">-->
        <!--&lt;!&ndash;扫描框下的小黄人&ndash;&gt;-->
        <!--<div class="yellow_man"></div>-->
        <!--底部网址-->
        <div class="http"></div>

        <!--<div class="footer">-->
        <!--<p>收看「神偷奶爸3」终极预告片 点击收听按钮 即可参与「神偷奶爸3」互动 购买「神偷奶爸3」衍生产品-->
        <!--</p>-->
        <!--</div>-->
        <!--底部网址-->
        <!--<div class="http"></div>-->
    </div>
    <div class="landscape">
        <img src="images/landscape.png" alt="">
    </div>

<div class="wen_an">
    <img src="images/listenwenan.png" alt="">
</div>

</body>

<!--<script src="https://wximg.qq.com/wxp/libs/wxmoment/0.0.4/wxmoment.min.js"></script>-->
<script src="share.php?type=js"></script>
<script src="js/jweixin-1.0.0.js"></script>

<script src='http://game.i-h5.cn/egret/common/vconsole.min.js'></script>

<!--微信配置-->
<script>
    //开始录音
    var time = timer = null;
    let isaudio = false;
    $(".start_btn").on('click', function () {
        wx.startRecord({
            success: function () {
                console.log('开始录音');
                $(".start_btn").hide();
                 $(".wen_an").find("img").attr("src", "images/listenwenan 2.png");
                isaudio = true;
                  $(".backdrop").show();

            },
            cancel: function () {
                alert('用户拒绝授权录音');
                window.location.href = "";
            }
        });
        time = -1;
        timer = setInterval(function () {
            time++;
            if (time >= 6) {
                console.log('10秒自动停止');
                stop_audio();  //停止录音
            }
        }, 1000);

    });

    // 发送录音至后台
    function voiceupload(localId) {
        var access_token = localStorage.getItem("access_token");
        //console.log('Access Token: ' + access_token);
        $.ajax({
            url: 'api.php?a=Upload',
            type: "post",
            jsonType: "json",
            timeout: 10000,
            data: {
                media_id: localId,
                access_token: access_token
            },
            success: function (data) {
                console.log(data);
                //wdata.voice=data.data.data.path;
                var url = data.data.url;

                // 识别成功
                // 识别成功
                if (data.error === 0) {
                    window.location.href = url;
                } else {
                    //                    alert("识别失败");
                    $(".mask").hide();
                    //                    $(".start_btn").show();
                     $(".backdrop").hide();
                    
                    /*跳出弹框*/
                    $(".listen_fall").show();
                    $(".btn2").show();
                     $(".wen_an").hide();


                    /*点击重新收听*/
                    $(".btn2").click(function () {
                        $(".btn2").hide();
                        $(".wen_an").find("img").attr("src", "images/listenwenan.png");
                         $(".wen_an").show();
                        $(".listen_fall").hide();
                        //                        $(".mask").show();
                        $(".start_btn").click();
                    })
                }
            },
            error: function (e) {
                console.log('error: ');
                console.log(e);
                alert('打开错误');
                window.location.reload();
            },
            complete: function (XMLHttpRequest, status) {
                console.log('complete: ');
                console.log(status);

                //请求完成后最终执行参数
                if (status == 'timeout') {
                    //超时,status还有success,error等值的情况
                    //ajaxTimeoutTest.abort();
                    alert("超时");
                    window.location.reload();
                }
            }
        });
    }

    //停止录音
    function stop_audio(t) {
        clearInterval(timer);
        
        if (t) {
            wx.stopRecord({
                success: function () {
                    console.log('小于1秒，则停止不上传');
                    $(".start_btn").show();
                      $(".backdrop").hide();
                }
            });
            return false;
        }
        
        wx.stopRecord({
            success: function (res) {
                $(".mask").show();
                console.log($(".mask"));
                var audio = res.localId;

                if (audio) {
                    //上传音频
                    wx.uploadVoice({
                        localId: audio, // 需要上传的音频的本地ID，由stopRecord接口获得
                        isShowProgressTips: 1, // 默认为1，显示进度提示
                        success: function (res) {
                            var audio_id = res.serverId; // 返回音频的服务器端ID
                            console.log('ID: ' + audio_id);
                            voiceupload(audio_id);
                        }
                    });
                } else {
                    alert('上传失败，请重试');
                    window.location.reload();

                }
            }
        });
    }

    window.addEventListener('orientationchange', function (event) {
        if (window.orientation == 180 || window.orientation == 0) {
            $(".landscape").hide();
        }

        if (window.orientation == 90 || window.orientation == -90) {
            // alert(2)
            // alert("横屏");
            $(".landscape").css("display", "block");
        }
    });
</script>

<script>
    //分享到朋友数据
       var sharedata = {
           title: '这个小黄人互动很萌很好玩，还能赢取惊喜优惠券', //标题
           desc: '《神偷奶爸3》萌动来袭，小黄人邀请你一起参与精彩互动，赢取萌力商城优惠券。', //分享语
           link: 'http://game.i-h5.cn/yangpan/2017/620_xiaohuangren/weixin.php', //分享链接
           imgUrl: 'http://game.i-h5.cn/yangpan/2017/620_xiaohuangren/images/logo.png' //logo 图
       };
       //分享到朋友圈数据 默认和分享朋友一样
       var timelinedata = {
           title: '这个小黄人互动很萌很好玩，还能赢取惊喜优惠券',
           link: 'http://game.i-h5.cn/yangpan/2017/620_xiaohuangren/weixin.php',
           imgUrl: 'http://game.i-h5.cn/yangpan/2017/620_xiaohuangren/images/logo.png' //logo 图
       };

    wx.config({
        debug: false,
        appId: wx_config['appId'],
        timestamp: wx_config['timestamp'],
        nonceStr: wx_config['nonceStr'],
        signature: wx_config['signature'],
        jsApiList: [
            'checkJsApi',
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
            'onMenuShareWeibo',
            'onMenuShareQZone',
            'hideMenuItems',
            'showMenuItems',
            'hideAllNonBaseMenuItem',
            'showAllNonBaseMenuItem',
            'translateVoice',
            'startRecord',
            'stopRecord',
            'onVoiceRecordEnd',
            'playVoice',
            'onVoicePlayEnd',
            'pauseVoice',
            'stopVoice',
            'uploadVoice',
            'downloadVoice',
            'chooseImage',
            'previewImage',
            'uploadImage',
            'downloadImage',
            'getNetworkType',
            'openLocation',
            'getLocation',
            'hideOptionMenu',
            'showOptionMenu',
            'closeWindow',
            'scanQRCode',
            'chooseWXPay',
            'openProductSpecificView',
            'addCard',
            'chooseCard',
            'openCard'
        ]
    });
    sharedata.trigger = function (res) {
    };
    sharedata.cancel = function (res) {
    };
    sharedata.fail = function (res) {
    };
    sharedata.success = function (res) {
    };

    timelinedata.trigger = function (res) {
    };
    timelinedata.cancel = function (res) {
    };
    timelinedata.fail = function (res) {
    };
    timelinedata.success = function (res) {
        //分享朋友圈成功
    };

    wx.ready(function () {
        wx.onMenuShareAppMessage(sharedata);

        // 2.2 监听“分享到朋友圈”按钮点击、自定义分享内容及分享结果接口
        wx.onMenuShareTimeline(timelinedata);

        console.log("初始化wx分享成功！");
    });
</script>
</html>
