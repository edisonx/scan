var time = timer = 0;
let isaudio = false;
let record_time = 4;
let ajax_time = 1;     // 请求延时

let dowm_time = 0;   // 减去10秒，因为从录音开始，但我们是从录音结束开始的


$(function () {
    let reset = GetQueryString('reset');
    if (reset) {
        window.location.href = "http://h5.edisonx.cn/718_audio/listen/index.html";
    }


    $(".footer_tab li").click(function () {
        let i = $(this).index();

        $(".footer_tab li a").css('color', '#000');
        $(this).find('a').css('color', '#ff7200');
        $(".footer_tab li").eq(0).find("img").attr("src", "assets/listen_1.png");
        $(".footer_tab li").eq(1).find("img").attr("src", "assets/shop_ico_0.png");
        $(".footer_tab li").eq(2).find("img").attr("src", "assets/gift_0.png");
        $(".footer_tab li").eq(3).find("img").attr("src", "assets/my_center_0.png");

        switch (i) {
            case 0:
                console.log(0);
                $(".footer_tab li").eq(0).find("img").attr('src', "assets/listen_0.png");
                break;
            case 1:
                console.log(1);
                $(".footer_tab li").eq(1).find("img").attr("src", "assets/shop_ico_1.png");
                break;
            case 2:
                console.log(2);
                $(".footer_tab li").eq(2).find("img").attr("src", "assets/gift_1.png");
                break;
            case 3:
                console.log(3);
                $(".footer_tab li").eq(3).find("img").attr("src", "assets/my_center_1.png");
                break;
        }
    });


});



//停止录音
function stop_audio(t) {
    wx.stopRecord({
        success: function (res) {
            // 停止完
            // $(".")
            $(".is_listen_ico").hide();
            $(".listen_loadding").removeClass();
            $(".listen_over").show();
            // 停止完上传
            var audio = res.localId;
            if (audio) {
                //上传音频
                wx.uploadVoice({
                    localId: audio, // 需要上传的音频的本地ID，由stopRecord接口获得
                    isShowProgressTips: 1, // 默认为1，显示进度提示
                    success: function (res) {
                        var audio_id = res.serverId; // 返回音频的服务器端ID
                        console.log('ID: ' + audio_id);
                        voiceupload(audio_id);
                    }
                });
            } else {
                console.log('上传失败，请重试');
                window.location.reload();
            }
        }
    });
}

// 发送录音至后台
function voiceupload(localId) {
    var access_token = localStorage.getItem("access_token");
    var startRecordTime = window.localStorage.getItem('startRecordTime');
    console.log(`localId:${localId}`);
    console.log(`access_token:${access_token}`);
    console.log(`curr_time:${startRecordTime}`);

    // 开始定时，看请求有几秒的延迟，然后加上延迟


    let ajax_app = setInterval(function () {
        ajax_time++;
    }, 1000);

    $.ajax({
        url: 'http://h5.edisonx.cn/718_audio/api.php?a=uploadAudio',
        type: "post",
        jsonType: "json",
        timeout: 10000,
        data: {
            media_id: localId,
            access_token: access_token,
            curr_time: startRecordTime
        },
        success: function (data) {
            clearInterval(ajax_app);  //清理定时器

            console.log(`请求延迟了${ajax_time}秒`); //请求延时
            ajax_time *= 1000;   //延时时间戳
            // 是否两个时间都加上这个延迟

            console.log('返回数据');
            console.log(data);
            // 识别成功
            if (data.error === 0) {
                // window.location.href = url;]\
                console.log('识别成功');
                //console.log('识别成功');
                console.log('返回信息');
                console.log(data);
                discern(data);
            } else {
                console.log('识别失败');
                $(".start_listen").show();
                $(".start_listen").show();
                $(".is_listen").hide();
                $(".head").css({
                    "background": "none;",
                    " box- shadow": "none;"
                });
                $(".listen_fail").show();
                //录音失败重新录音
                wx.startRecord({
                    success: function () {
                        console.log("录音失败重新录音");
                        // 从录音时间开始
                        let now_time = new Date().getTime();
                        window.localStorage.setItem('startRecordTime', now_time);

                        $(".start_listen").hide();
                        $(".listen_over").hide();
                        $(".is_listen").show();
                        $(".head").css({
                            "background": "#fff;",
                            " box- shadow": " 0 0.5vh 1vh #ccc;"
                        });
                        // $(".listen_fail").hide();
                        $(".is_listen_ico").show();
                        $(".is_listen span").addClass("listen_loadding");
                        var timr = setInterval(function () {
                            if (timer >= record_time) {
                                // 大于5秒自动停止
                                clearInterval(timr);
                                timer = 0;
                                console.log(timer);
                                stop_audio(timer);
                            } else {
                                timer++;
                            }
                        }, 1000);
                    },
                    cancel: function () {
                        console.log('用户拒绝授权录音');
                        window.location.href = "";
                    }
                });
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log('error');
            console.log(XMLHttpRequest.status);
            console.log(XMLHttpRequest.readyState);
            console.log(textStatus);
        },
        complete: function (XMLHttpRequest, status) {
            console.log('complete: ');
            console.log(status);

            //请求完成后最终执行参数
            if (status == 'timeout') {
                //超时,status还有success,error等值的情况
                //ajaxTimeoutTest.abort();
                console.log("超时");
                window.location.reload();
            }
        }
    });
}

// 识别成功
function discern(data) {
    /*
        1.电影场次传到后台
        2.拿到当前时间，做判断，看现在时候游戏已经开始
        3.跳转到游戏页面，吧两个时间值带过去
    */
    console.log('返回信息');
    console.log(data);
    // let now_time = new Date().getTime();  // 时间戳
    let sceneID = data.data.sceneID;  //电影场次
    // let time1 = data.data.actions[0].startTime;  //游戏1时间
    // let time1_end = data.data.actions[0].endTime;  //游戏1结束时间
    // let time2 = data.data.actions[1].startTime;  // 游戏2时间
    // let time2_end = data.data.actions[1].endTime;  // 游戏2结束时间
    let url1 = data.data.url;        //url
    //let url1 = "http://moto-question.izhida.cn/";
    let t3 = [];
    // if (data.data.actions.length > 2) {
    for (let x = 0; x < data.data.actions.length; x++) {
        t3.push(data.data.actions[x].startTime + "|" + data.data.actions[x].endTime);  //开始时间和结束时间||
    }
    // }
    t3 = t3.join("-");     //每个游戏之间用--分割
    // 加上请求延迟，然后减去 误差时间
    // 如果手机比视屏快，就减
    // time1 += 1000;
    // time1_end += 1000;
    // time1 -= dowm_time;

    var storage = window.localStorage;
    console.log(storage);
    let {nickname, name, sex, city, country, openid, headimgurl, user_id, mobile_is} = storage;
    //console.log(`headimgurl:${headimgurl}`);
    let option = `time=${t3}&sceneID=${sceneID}&nickname=${nickname}&sex=${sex}&city=${city}&country=${country}&openid=${openid}&headimgurl=${headimgurl}&user_id=${user_id}&mobile_is=${mobile_is}`;
    option = encodeURIComponent(option);
    console.log(`option:${option}`);
    /*
        1.如果现在游戏1还没结束，则跳到游戏1，带时间过去
        2.如果现在游戏2还没结束，则跳到游戏2，带时间过去
        3.如果游戏都够了，则弹窗提示，游戏结束
    */
    // let temp_time = (time1 - now_time) / 1000;
    // console.log(`时间差:${temp_time}秒数`);
    console.log(`url:${url1}?${option}`);
    // return false;
    window.location.href = `${url1}?${option}`;
    // return false;
    // if (time1_end > now_time) {
    // console.log('跳转到游戏1');
    // window.location.href = `${url1}?time1=${time1}&&time2=${time2}&time3=${t3}&&sceneID=${sceneID}&nickname=${nickname}&sex=${sex}&city=${city}&country=${country}&openid=${openid}&imgURL=${imgURL}`;
    // } else if (time2_end > now_time) {
    // console.log('跳转到游戏2');
    // window.location.href = `http://h5.edisonx.cn/718_audio/moto/index.html?time2=${time2}&&sceneID=${sceneID}`;
    // } else {
    //     console.log(`游戏结束`);
    //     console.log(`游戏1开始:${time1},结束时间${time1_end}`);
    //     console.log(`游戏2开始:${time2},结束时间${time2_end}`);
    //     window.location.href = `http://h5.edisonx.cn/718_audio/moto/index.html?gameover=${true}`;
    // }
    // console.log(`now_time${now_time}`);
    // console.log(`sceneID:${sceneID}`);
    // console.log(`time1:${time1}`);
    // console.log(`time2:${time2}`);
}


function GetQueryString(name) {
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    let r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
