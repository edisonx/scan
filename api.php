<?php
//ini_set("session.save_handler", "memcache");
//ini_set("session.save_path", "tcp://d02f7ef2dca84c46.m.cnszalist3pub001.ocs.aliyuncs.com:11211");
session_start();
error_reporting(E_ERROR);
date_default_timezone_set('Asia/Chongqing');
include './php/function.php';
include './php/database.class.php';
$config=include './php/config.php';
global $Db;
$Db=new database($config);
header('Access-Control-Allow-Origin:*');
$openid=$_SESSION['openid'];
$userinfo	= $_SESSION['userinfo'];
//防止未及时关闭测试
if(empty($openid) && $config['debug']){
	$openid='ocGKMwc1woatRFR7Iu0BvhXq51xU';
}

$type=$_REQUEST['a']?:'start';

if(empty($openid) and $type != 'silkToMp3'){
    showerr('未获取到用户openid');
}

// 测试
//$_SESSION['audio_mark'] = 1;
//$_SESSION['nickname'] = 2;
//$_SESSION['headimgurl'] = 3;

// 奖品配置表
$prize_list = array(
    /* 1 => [
        'grade'=>1, 
        'name'=>'万达电影票', 
        'desc'=>'恭喜您获得一等奖', 
        'coupon_id' => 16,
        'expire'=>'有效期截止至2017年9月1日',
        'way'=>'观影结束后，前往Edisonx礼台，凭上方兑换码领取奖品。',
        'url'=>'http://www.91qzb.com/mobile',
        'addr'=>'万达影城石景山店',
        'exprie_date'=>'2017年9月1日',
        'img' => '', // 公众号图片
        'exprie_unix' => '1504281599',
    ],
    2 => [
        'grade'=>2, 
        'name'=>'精美电影手办', 
        'desc'=>'恭喜您获得二等奖',
        'coupon_id' => 17,
        'expire'=>'有效期截止至2017年9月1日',
        'way'=>'观影结束后，前往Edisonx礼台，凭上方兑换码领取奖品。',
        'url'=>'http://www.91qzb.com/mobile',
        'addr'=>'万达影城石景山店',
        'exprie_date'=>'2017年9月1日',
        'img' => '', // 公众号图片
        'exprie_unix' => '1504281599',
    ], */
    1 => [
        'grade'=>3,
        'name'=>'¥20代金券',
        'desc'=>'恭喜您获得Edisonx商城20元代金券一张',
        'coupon_id' => 18,
        'expire'=>'有效期2017-08-07至2018-08-06',
        'way'=>'使用地址 http://www.91qzb.com',
        'url'=>'http://www.91qzb.com/mobile',
        'addr'=>'使用地址：http://www.91qzb.com',
        'exprie_date'=>'2018年8月6日',
        'img' => '', // 公众号图片
        'exprie_unix' => '1533571199',
    ],
    2 => [
        'grade'=>3,
        'name'=>'¥20代金券',
        'desc'=>'恭喜您获得Edisonx商城20元代金券一张',
        'coupon_id' => 18,
        'expire'=>'有效期2017-08-07至2018-08-06',
        'way'=>'使用地址 http://www.91qzb.com',
        'url'=>'http://www.91qzb.com/mobile',
        'addr'=>'使用地址：http://www.91qzb.com',
        'exprie_date'=>'2018年8月6日',
        'img' => '', // 公众号图片
        'exprie_unix' => '1533571199',
    ],
    3 => [
        'grade'=>3, 
        'name'=>'¥20代金券', 
        'desc'=>'恭喜您获得Edisonx商城20元代金券一张',
        'coupon_id' => 18,
        'expire'=>'有效期2017-08-07至2018-08-06',
        'way'=>'使用地址 http://www.91qzb.com',
        'url'=>'http://www.91qzb.com/mobile',
        'addr'=>'使用地址：http://www.91qzb.com',
        'exprie_date'=>'2018年8月6日',
        'img' => '', // 公众号图片
        'exprie_unix' => '1533571199',
    ],
);

try{
    // 每个小时根据创建时间来更新排名信息 - 两个小时后计算用户排名
    
    
    /**
     * 接收分类
     */
    if ($type == 'submitScore')
    {
        // time_slot: 电影场次
        if (!isset($_POST['type']) or !isset($_POST['score']) or !isset($_POST['time_slot'])) {
            showerr('参数错误');
        }
        
        // 用于区分用户游戏场次
        $time_slot = $_POST['time_slot'];
        $date = date('Y-m-d H:i:s');
        $score = $_POST['score'];   // 分数
    
        // 第一次游戏更新数据
        if ($_POST['type'] == 'racing') {            
            $sql = "update 201707_audio_game
                    set score_one = $score,
                        create_time_one = '$date',
                        score_total = $score
                    where openid = '$openid' and time_slot = '$time_slot'";
            
            $res = $Db->execute ($sql);
            
            if (!$res) {
                showerr();
            }
            
            showsuc();
        }        
        
        //第二次游戏更新数据 - 摩托罗拉
        if ($_POST['type'] == 'moto') {
            // 更新第二次游戏数据和总游戏分数
            $sql = "update 201707_audio_game 
                    set score_two = $score, 
                        create_time_two = '$date', 
                        score_total = score_total + $score
                    where openid = '$openid' and time_slot = '$time_slot'";
            
            $res = $Db->execute ($sql);
            
            if (!$res) {
                showerr();
            }
            
            showsuc();
        }
        
    }
    
    /**
     * 获取用户最后一次，所在场次的排行信息
     * 第一名1等奖，是万达电子影票一张。
     * 2-3名是2等奖，奖品为实物手办一个。
     * 4-全部人是3等奖，奖品为5元Edisonx商城代金券。
     */
    if ($type == 'getOrder')
    {
        // 获取当前用户最后一次游戏数据
        $user = $Db->getOne("select * from 201707_audio_game where openid='$openid' order by id desc");
        
        // 获取本场次前10名
        $time_slot = $user['time_slot'];
        $res = $Db->getAll("select nickname,avatarurl,score_total,create_time_one from 201707_audio_game 
                            where time_slot = '$time_slot' group by openid 
                            order by score_total desc, create_time_two limit 10");
        
        // 计算当前用户本场次排名
        $my_score = $user['score_total'];
        $myRanking = $Db->getOne("select count(*) as count from 201707_audio_game 
                            where score_total >= $my_score and time_slot = '$time_slot'");
        
        $data = [
            'my_ranking' => $myRanking['count'],
            'ranking_list' => $res,
        ];
        
        showsuc('成功', $data);
    }
    
    
    // 发送微信模板消息
    // crontab - 每1小时执行一次
    if($type == 'wxSentTemplateMsg') {
        file_put_contents('test.txt', date('Y-m-d H:i:s') . PHP_EOL, FILE_APPEND );
        
        ##获取token
        $access_json = file_get_contents('http://www.91qzb.com/thinkphp/public/index.php/api/index/gzhGetAccessToken');
        $access = json_decode($access_json, true);
        $access_token = $access['access_token'];
        
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=$access_token";

        //$res = db - 当前时间  >= 创建时间 + 2小时 
        $res = $Db->getAll("select id, openid, nickname, prize_grade, mobile from 201707_audio_game 
                            where ticket_no <> ''
                                and push_time is null 
                                and unix_timestamp(create_time_two) + 7200 <= unix_timestamp()");
        
        foreach ($res as $key => $val) {
            $data = [
                'touser' => $val['openid'],
                'template_id' => 'fwqzxrTyQBugHq5UkESEvoT-Xs3DP4hZnHk4l1HfbzU',
                'url' => $prize_list[$val['prize_grade']]['url'],
                'data' => [
                    // 您有一张兑换券尚未使用
                    'first' => [
                        'value' => '您有一张兑换券尚未使用',
                        'color' => '#222'
                    ],
                    'cardNumber' => [
                        'value' => $val['mobile'],
                        'color' => '#222'
                    ],
                    'address' => [
                        'value' => $prize_list[$val['prize_grade']]['addr'],
                        'color' => '#222'
                    ],
                    'VIPName' => [
                        'value' => removeEmoji($val['nickname']),
                        'color' => '#222'
                    ],
                    'VIPPhone' => [
                        'value' => $val['mobile'],
                        'color' => '#222'
                    ],
                    'expDate' => [
                        'value' => $prize_list[$val['prize_grade']]['exprie_date'],
                        'color' => '#222'
                    ],
                    'remark' => [
                        'value' => '',
                        'color' => '#222'
                    ]
                ],
            ];
            
            $data = json_encode($data);
            $output = curlPost($url, $data);
            $output = json_decode($output, true);
            
            if ($output['errcode'] > 0) {
                $update = $Db->update(
                    '201707_audio_game',
                    array(
                        'status' => 2,
                        'push_time' => date('Y-m-d H:i:s'),
                        'comment' => $output['errmsg'],
                    ),
                    "id = " . $val['id']
                );
            }
            
            if ($output['errcode'] === 0) {
                $update = $Db->update(
                    '201707_audio_game',
                    array(
                        'status' => 1,
                        'push_time' => date('Y-m-d H:i:s'), 
                    ),
                    "id = " . $val['id']
                );
            }
            
        }
        
        // 43004 - require subscribe hint - 没有关注
        exit();
    }
        
	// 下载微信语音,然后上传到服务器识别,再返回识别后的信息
	if($type== 'uploadAudio'){
		$media_id 		= $_REQUEST["media_id"];
		$access_token 	= $_SESSION['access'];
		$curr_time = $_REQUEST['curr_time'];
      
		//$access_token	="t026lwTFSy_px1fV_rqJhSVJP8bc_38JBZe727VGgL0rst5dto5cBjMQ";
		$path = "weixinrecord/";  
		
		if(!is_dir($path)){
			mkdir($path);
		}
		
		//$access_token = '5MzU6_cm78_meXdek6sA-KCo6KExrslkEcXj3C_0DB2jrPjsKYksntvdmz_juhgM8tCOdz-m0qSQN1ht1OvA0-VlZsu54AaRAlDr9belKGnqf355NR0sMqU0iBiPOsHTYUOeACAFBV';
		
		//微 信上传下载媒体文件
		$url = "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token={$access_token}&media_id={$media_id}";
		
        $filename 		= "wxupload_".time().rand(1111,9999);
		$name     		= "$filename.amr";
		$saveFileName   = "$filename.amr";
		$result         = downAndSaveFile($url, $path."/".$name, $path."/".$saveFileName, $media_id);

        // 要上传的本地文件地址"@F:/xampp/php/php.ini"上传时候，上传路径前面要有@符号@E:/phpStudy/WWW/Intel/weixinrecord/1234.wav
        $furl = '@' . $path .  $saveFileName; 
        
        // 测试
        //$furl = '@weixinrecord/2.amr';
        $post_data = array (
            "wechartAudio" => $furl,
            "curr_time" => $curr_time
        );
        //CURLOPT_URL 是指提交到哪里？相当于表单里的“action”指定的路径
        $url = "http://101.201.38.200:8080/web/voiceMatch/voiceMatch.do";
                
        $output = curlPost($url, $post_data);
        //showerr('suc', $output);
        //var_dump($post_data);
        //var_dump($output);die;
        //file_put_contents('test.txt', date('Y-m-d H:i:s') . ' [uploadAudio] ' . $output . PHP_EOL, FILE_APPEND );
        
        $output = json_decode($output, true);
        
        // 失败
        if (!isset($output['status'])) {
            showerr('', $output);
        }
        
        if ($output['status'] != 1) {
            showerr('', $output['error']);
        }        
        
        $data = $output['data'][0];
        
        /* if ($data['match'] < 60) {
            showerr('识别失败', $data);
        } */
        
        // 收听就写入数据
        $insertData = [];
        $insertData['openid'] = $openid;
        $insertData['status'] = 0;
        $insertData['nickname'] = removeEmoji($_SESSION['nickname']);
        $insertData['avatarurl'] = $_SESSION['headimgurl'];
        $insertData['create_time'] = date('Y-m-d H:i:s');
        $insertData['time_slot'] = $data['sceneID'];
        
        $res = $Db->add('201707_audio_game', $insertData);
        
        if (empty($data)) {
            showerr('识别失败');
        }
        
        // 首次增加10条假数据
        // 假数据，nickname,avatarurl,score_total,create_time_one
        $userinfos = $Db->getAll("select openid, nickname, headimgurl from game_user_info order by rand() limit 10");

        foreach ($userinfos as $key => $value) {
            $fake = [];
            $fake['status'] = 1;
            $fake['type'] = 'fake';
            $fake['openid'] = $value['openid'];
            $fake['nickname'] = removeEmoji($value['nickname']);
            $fake['avatarurl'] = $value['headimgurl'];
            $fake['create_time_two'] = date('Y-m-d H:i:s');
            $fake['create_time'] = date('Y-m-d H:i:s');
            $fake['score_total'] = mt_rand(100, 200);
            $fake['time_slot'] = $data['sceneID'];

            $res = $Db->add('201707_audio_game', $fake);
        }
        
        showsuc('识别成功', $data);
	}
	
	## 核销
	// 核销 - 提交
	if ($type == 'chargeOffSubmit') {
	    $ticket_no = $_POST['ticket_no'];
	    $date = date('Y-m-d H:i:s');
	    
	    if (empty($ticket_no)) {
	        showerr('兑换码不正确，请重新输入');
	    }
	    
	    $res = $Db->getOne("select id,shop_name,nickname,avatarurl,mobile,ticket_no,exchange_time,prize_grade from 201707_audio_game where ticket_no = '$ticket_no'");
	    
	    if (!$res) {
	        showerr('兑换码不正确，请重新输入');
	    }
	    
	    if (!empty($res['exchange_time'])) {
	        showerr('兑换失败，此券已经兑换', $res);
	    }
	    
	    // 过期
	    if ($prize_list[$res['prize_grade']]['exprie_unix'] < time()) {
	        showerr('此券已过期');
	    }
	    
	    // 商派 - 券表
	    $Db->update(
            'ecm_coupon_sn',
	        array('remain_times'=>0, 'status'=>1),
	        "coupon_sn = $ticket_no"
        );
	    
	    // 游戏表
	    $update = $Db->update(
	        '201707_audio_game', 
	        array('exchange_time'=>$date), 
	        "id = " . $res['id']
        );
	    
	    if (!$update) {
	        showerr('核销失败');
	    }
	    
	    $res['exchange_time'] = $date;
	    showsuc('核销成功', $res);
	}
	
	// 核销 - 撤销兑换
	if ($type == 'chargeOffRepeal') {
	    $ticket_no = $_POST['ticket_no'];
	     
	    if (empty($ticket_no)) {
	        showerr('兑换码错误');
	    }
	     
	    $res = $Db->getOne("select id,shop_name,nickname,avatarurl,mobile,ticket_no,exchange_time from 201707_audio_game where ticket_no = '$ticket_no'");
	     
	    if (!$res) {
	        showerr('兑换码错误');
	    }
	    
	    if (empty($res['exchange_time'])) {
	        showerr('已经撤销');
	    }
	    
	    // 商派 - 券表
	    $Db->update(
	        'ecm_coupon_sn',
	        array('remain_times'=>1, 'status'=>0),
	        "coupon_sn = $ticket_no"
        );
	    
	    //
	    $update = $Db->update(
	        '201707_audio_game',
	        array('exchange_time'=>NULL),
	        "id = " . $res['id']
        );
	     
	    if (!$update) {
	        showerr('撤销失败');
	    }
	    
	    showsuc('撤销成功', $res);
	}
	
	// 核销 - 查看兑换记录
	if ($type == 'chargeOffRecord') {
	    $res = $Db->getAll("select id,shop_name,nickname,avatarurl,mobile,ticket_no,exchange_time from 201707_audio_game where exchange_time <> '' order by exchange_time desc limit 100");
	    
	    if (!$res) {
	        showerr('数据为空');
	    }
	    
	    showsuc('成功', $res);
	}
	
	// 核销 - 搜索
	if ($type == 'chargeOffSearch') {
	    $where = '1 = 1';
        $mobile = $_POST['mobile'];
        $shop_name = $_POST['shop_name'];
        $exchange_time = $_POST['exchange_time'];
	    
	    if (empty($mobile) and empty($shop_name) and empty($exchange_time)) {
	        showerr('搜索数据不能为空');
	    }
	    
	    if (!empty($mobile)) {
	        $where .= " and mobile = $mobile";
	    }
	    
	    if (!empty($shop_name)) {
	        $where .= " and shop_name like '%$shop_name%'";
	    }
	    
	    // 年月日
	    if (!empty($exchange_time)) {
	        $where .= " and exchange_time like '$exchange_time%'";
	    }
	       
	    $where .= " and exchange_time <> ''";
	    
	    $res = $Db->getAll("select id,shop_name,nickname,avatarurl,mobile,ticket_no,exchange_time from 201707_audio_game where $where order by exchange_time desc limit 100");
	    
	    if (!$res) {
	        showerr('无数据');
	    }
	    
	    showsuc('成功', $res);
	}
	
	
    ## 公众号

    // 判断是否参与游戏
    if ($type == 'gzhGetTicket') {
        # 判断是否参与游戏
        $res = $Db->getOne("select * from 201707_audio_game where openid = '$openid'");
        
        if (!$res) {
            showerr('no game');
        }
        
        # 判断是否有未领取的券
        $game = $Db->getOne("select * from 201707_audio_game where openid = '$openid' and ticket_get_date is null order by create_time_two desc");
        
        // 如果玩过游戏，但没有可领取的券，就显示最后一条数据
        if (!$game) {
            $ticket_is_not = 1; // 标记：没有可领券的数据
            $game = $Db->getOne("select * from 201707_audio_game where openid = '$openid' and ticket_get_date is not null order by create_time_two desc");
        }
        
        // 查询获取的奖励
        // 计算当前用户本场次排名
        // 获取用户最后一次，所在场次的排行信息
        $time_slot = $game['time_slot'];
        $ranking = $Db->getAll("select openid,avatarurl,nickname from 201707_audio_game where time_slot = '$time_slot' group by openid order by score_total desc, create_time_two asc limit 10");
        
        $my_ranking = 0;
        
        foreach ($ranking as $key => $value) {
            if ($value['openid'] == $openid) {
                $my_ranking = $key + 1;
                break;
            }
        }
        
        // 计算奖品
        if ($my_ranking == 1) {
            $prize =  $prize_list[1];
        } elseif ($my_ranking >= 2 and $my_ranking <= 3) {
            $prize = $prize_list[2];
        } else {
            //$prize = $Db->getOne("select * from ecm_coupon where id = 18");
            $prize = $prize_list[3];
        }
        
        # 个人排名 
        $my_score = $game['score_total'];
        $res_ranking = $Db->getOne("select count(*) as count from 201707_audio_game where score_total >= $my_score and time_slot = '$time_slot'");
        
        # 防止1234名有误差
        if ($my_ranking > 4) {
            $my_ranking = $res_ranking['count'];            
        }
        
        # 如果用户已经游戏并且券已全部领完，就显示最后一条数据，返回给前端显示 
        if (isset($ticket_is_not)) {
            $prize = $prize_list[$game['prize_grade']];
            $prize['ticket_no'] = $game['ticket_no'];
            $prize['ranking_list'] = $ranking;
            $prize['ranking_my'] = $my_ranking;
            showsuc('success', $prize);
        }
        
        # 判断是否填写手机
        $third = $Db->getOne("select * from ecm_third_login where openid = '$openid'");
        $member = $Db->getOne("select * from ecm_member where user_id = {$third['user_id']}");
        
        //var_dump($member);die;
        // 手机号为空
        if (empty($member['phone_mob'])) {
            $prize['ranking_list'] = $ranking;
            $prize['ranking_my'] = $my_ranking;
            showerr('mobile empty', $prize);
        }
        
        $ticket_no = time() . mt_rand(10000, 99999);
        
        // 为用户创建券 - 实物券与虚拟券
        
        # 和商派打通券功能 ???
        $data = array();
        $data['coupon_sn'] = $ticket_no;
        $data['coupon_id'] = $prize['coupon_id'];
        $data['remain_times'] = 1;
        $data['user_id'] = $member['user_id'];
        $data['ticket_get_date'] = date('Y-m-d H:i:s');
        $Db->add('ecm_coupon_sn', $data);
        
        // 更新游戏表中的券
        $update = $Db->update(
            '201707_audio_game',
            array(
                'prize_grade'=>$prize['grade'],
                'prize_name'=>$prize['name'], 
                'shop_name'=>$prize['name'], 
                'ticket_no'=>$ticket_no, 
                'ticket_get_date'=>date('Y-m-d H:i:s'), 
                'mobile'=>$member['phone_mob'],
            ),
            "id = " . $game['id']
        );
        
        if (!$update) {
            showerr('券创建失败');
        }
        
        $prize['ranking_list'] = $ranking;
        $prize['ranking_my'] = $my_ranking;
        $prize['ticket_no'] = $ticket_no;
        
        showsuc('success', $prize);
        // 发模板消息状态为等待发送
    }
    
    // 短信验证码
    if ($type == 'gzhSendNodeCode' or $type == 'sendNodeCode') {
        $mobile = $_POST['mobile'];
        
        if (!isMobile($mobile)) {
            showerr('mobile error');
        }
        
        $code = mt_rand(1000, 9999);
        
        // 测试
        //$code = 1234;
        
        $member = $Db->getOne("select * from ecm_member where phone_mob = '$mobile'");
        
        if ($member) {
            showerr('mobile already exist');
        }
        
        $_SESSION['audio_mobile'] = $mobile;
        $_SESSION['audio_note_code'] = $code;
        
        $content = "【喜乐迷】您的短信验证码是" . $code . "。该验证码有效期15分钟。请在页面中输入以完成验证。";
        
        $res = sentNodeMsg($mobile, $content);
        
        if (!$res) {
            showerr('node send fail');
        }
        
        // 加密用于绑定手机号，验证
        $md5 = md5($code . $config['secret'] . $mobile);
        
        $mstr = base64_encode($md5 . '-' . $mobile);
        
        showsuc('success', array('mstr'=>$mstr));
    }
    
    // 验证短信码
    if ($type == 'gzhVerifyCode') {
        $code = $_POST['code'];
        $mobile = $_SESSION['audio_mobile'];
        
        if (empty($code)) {
            showerr('验证码为空');
        }
        
        if ($code != $_SESSION['audio_note_code']) {
            showerr('验证码错误');
        }
        
        // 判断是否参与游戏
        $res = $Db->getOne("select * from 201707_audio_game where openid = '$openid' and ticket_get_date is null order by create_time_two desc");
        
        if (!$res) {
            showerr('没有可以领取的券');
        }
        
        // 查询获取的奖励
        // 计算当前用户本场次排名、奖品
        // 获取用户最后一次，所在场次的排行信息
        $time_slot = $res['time_slot'];
        $ranking = $Db->getAll("select * from 201707_audio_game where time_slot = '$time_slot' group by openid order by score_total desc, create_time_two asc limit 10");
        
        $my_ranking = 0;
        
        foreach ($ranking as $key => $value) {
            if ($value['openid'] == $openid) {
                $my_ranking = $key + 1;
                break;
            }
        }
        
        if ($my_ranking == 1) {
            $prize =  $prize_list[1];
        } elseif ($my_ranking >= 2 and $my_ranking <= 3) {
            $prize = $prize_list[2];
        } else {
            //$prize = $Db->getOne("select * from ecm_coupon where id = 18");
            $prize = $prize_list[3];
        }
        
        // 查询user_id
        $third = $Db->getOne("select * from ecm_third_login where openid = '$openid'");
        
        if (!$third) {
            showerr('openid没有创建');
        }
        
        $pwd = mt_rand(100000, 999999);
        
        // 更新手机号到用户账号
        $update = $Db->update(
            'ecm_member',
            array(
                'phone_mob' => $mobile,
                'password' => md5($pwd)
            ),
            "user_id = {$third['user_id']}"
        );
        
        $ticket_no = time() . mt_rand(10000, 99999);
        
        // 为用户创建券 - 实物券与虚拟券
        
        // 虚拟券和商派打通
        $data = array();
        $data['coupon_sn'] = $ticket_no;
        $data['coupon_id'] = $prize['coupon_id'];
        $data['remain_times'] = 1;
        $data['user_id'] = $third['user_id'];
        $data['ticket_get_date'] = date('Y-m-d H:i:s');
        $Db->add('ecm_coupon_sn', $data);
    
        //
        $update = $Db->update(
            '201707_audio_game',
            array(
                'prize_grade'=>$prize['grade'],
                'prize_name'=>$prize['name'],
                'shop_name'=>$prize['name'],
                'ticket_no'=>$ticket_no,
                'ticket_get_date'=>date('Y-m-d H:i:s'),
                'mobile'=>$mobile,
            ),
            "id = " . $res['id']
        );
        
        $content = "【喜乐迷】您的登陆账号为手机号，登陆初始密码为" . $pwd . "。";
        
        $res = sentNodeMsg($mobile, $content);
        
        if (!$update) {
            showerr('券创建失败');
        }
        
        $prize['ticket_no'] = $ticket_no;
        showsuc('验证码正确', $prize);
    }
    
    // 获取券信息
    if ($type == 'gzhGetTicketDetail') {
        $ticket_no = $_POST['ticket_no'];
        
        if (empty($ticket_no)) {
            showerr('券ID错误');
        }
        
        $res = $Db->getOne("select id, ticket_no, prize_grade, ticket_get_date from 201707_audio_game where ticket_no = $ticket_no");
        
        if (!$res) {
            showerr('券不存在');
        }
        
        $prize = $prize_list[$res['prize_grade']];
        $prize['ticket_no'] = $res['ticket_no'];
        $prize['ticket_get_date'] = $res['ticket_get_date'];
        showsuc('success', $prize);
    }
    
    // 测试发送验证码
    if ($type == 'test') {
        $arr = json_decode('{"status":"1","error":null,"time":null,"data":[]}', true);
        var_dump($arr);
        
        if (empty($arr['data'][0])) {
            echo 1;
        }
        die;
        //file_put_contents('test.txt', date('Y-m-d H:i:s') . PHP_EOL, FILE_APPEND );
    }
    
	#############################
    #         公用接口          #
    #############################
    
    // 获取coupon接口
    if ($type == 'getCoupon') {
        // 多个coupon用逗号分割如：18,19
        $coupons = $_POST['coupons'];
        $user_id = $_POST['user_id'];
        
        if (empty($coupons) or empty($user_id)) {
            showerr('param error');
        }
                
        $coupons = str_replace('，', ',', $coupons);
        $coupons = rtrim($coupons, ',');
        $arr = explode(',', $coupons);
                
        # 查询用户是否存在
        $user = $Db->getOne("select * from ecm_member where user_id = $user_id");
        
        if (!$user) {
            showerr('user not exist');
        }
        
        // 手机号为空
        if (empty($user['phone_mob'])) {
            showerr('mobile unbound');
        }
        
        // 查询券ID是否存在
        $res = $Db->getAll("select * from ecm_coupon where coupon_id in ($coupons)");
        
        if (!$res) {
            showerr('param error');
        }
        
        // 如果券不存在
        if (count($arr) != count($res)) {
            showerr('param error');
        }
        
        # 创建券
        $number = 0;
        foreach ($arr as $key => $val) {
            $ticket_no = time() . mt_rand(10000, 99999);
                
            # 和商派打通券功能 ???
            $data = array();
            $data['coupon_sn'] = $ticket_no;
            $data['coupon_id'] = $val;
            $data['remain_times'] = 1;
            $data['user_id'] = $user_id;
            $data['ticket_get_date'] = date('Y-m-d H:i:s');
            $Db->add('ecm_coupon_sn', $data);
            
            $return_data[$number]['coupon_id'] = $val;
            $return_data[$number]['coupon_sn'] = $ticket_no;
            $number ++;
        }
        
        showsuc('success', $return_data);
    }
    
    // 绑定手机号
    if ($type == 'bindMobile') {
        $code = $_POST['code'];
        $openid = $_POST['openid'];
        $mstr = $_POST['mstr'];
        $mstr_arr = explode('-', base64_decode($mstr));
        $mobile = $mstr_arr[1];
        
        if (empty($openid)) {
            showerr('openid error');
        }
        
        if (empty($code) or $mstr_arr[0] != md5($code . $config['secret'] . $mobile)) {
            showerr('code error');
        }
        
        bindMobile($openid, $mobile, $code);
    }
    
    if ($type == 'silkToMp3') {
        $curr_time = getMillisecond();
                
        // 获取文件
        //$filename =  $_FILES['data']['name'];
        
        // 微信小程序录音后上传的silk文件是非base64编码
        // 名称
        $name = 'applet_' . uniqid() . mt_rand(10000000, 99999999);
        $uploadfile = $name . '.silk';  // 自定义上传文件名称
        $path_filename = 'uploads_silk/' . $uploadfile; // 上传的路径 + 文件名称
        //$path_filename = 'applet_59b63595708b124282756.silk';
        
        // 移动文件到指定目录
        //move_uploaded_file($_FILES['data']['tmp_name'], $path_filename);
     
        // silk转mp3
        $commend = "bash converter.sh applet_59b63595708b124282756.silk wav";
        $res = system($commend, $retval);
        
        var_dump($retval);
        die;
        // 转换失败
        if (strcmp($retval, 0) != 0) {
            
        } else {
        // 转换成功后，删除silk文件    
            //unlink($uploadfile);
        }
        
        // 上传文件给识别接口
        $post_data = array (
            "wechartAudio" => '@' . $name . '.amr',
            "curr_time" => $curr_time
        );
        
        var_dump($post_data);
        //CURLOPT_URL 是指提交到哪里？相当于表单里的“action”指定的路径
        $url = "http://101.201.38.200:8080/web/voiceMatch/voiceMatch.do";
                
        $output = curlPost($url, $post_data);
        
        var_dump($output);
        
        $output = json_decode($output, true);
        
        // 失败
        if ($output['status'] != 1) {
            showerr('', $output['error']);
        }        
        
        $data = $output['data'][0];
        
        showsuc('识别成功', $data);
        exit();
    }
    
    
}catch (Exception $e){
	showerr($e->getMessage());
}

    //根据URL地址，下载文件
    function downAndSaveFile($url,$savePath,$saveFileName, $media_id){
        ob_start();
        
        $rf = readfile($url);
        
        if ($rf <= 1000) {
            //$file_get = file_get_contents($url);
            //$rf_arr = json_decode($file_get, true);
            
            // access过期,重新获取
            //if (isset($rf_arr['errcode'])) {
                //if ($rf_arr['errcode'] > 0) {
                    //var_dump($rf);die;
                    $access_json = file_get_contents('http://www.91qzb.com/thinkphp/public/index.php/api/index/gzhGetAccessToken?forceReset=1');
                    $access = json_decode($access_json, true);
                    $access_token = $_SESSION['access'] = $access['access_token'];
                    
                    $url = "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token={$access_token}&media_id={$media_id}";
                    
                    ob_clean();
                    
                    $rf = readfile($url);
                //}
            //}
        }
        
        if (!$rf) {
            showerr('读入文件并写入到输出缓冲失败');
        }
        
        $img  = ob_get_contents();
            
        if (!$img) {
            showerr('输出缓冲区无效');
        }
        
        ob_end_clean();
        
        $size = strlen($img);
        $fp = fopen($savePath, 'a');
        $fw = fwrite($fp, $img);
        fclose($fp);
        
        if (!$fw) {
            showerr('缓冲区内容写入失败');
        }
        
        // 不需要转换文件格式 
        return true;
        
        $command = "sox $savePath $saveFileName";
        system($command,$error);
        if($error){
            return false;
        }else{
            return true;
        }
    }
    
    // 发送手机短信
    function sentNodeMsg($mobile, $content)
    {
        if (empty($mobile)){
            return '请输入手机号码';
        }
        
        if (empty($content)){
            return '请填写内容';
        }
        
        $arr = [
            '-2'=>'账号密码错误',
            '-4'=>'余额不足支持本次发送',
            '-5'=>'数据格式错误',
            '-6'=>'参数有误',
            '-7'=>'权限受限',
            '-8'=>'流量控制错误',
            '-9'=>'扩展码权限错误',
            '-10'=>'内容长度长',
            '-11'=>'内部数据库错误',
            '-12'=>'序列号状态错误',
            '-14'=>'服务器写文件失败',
            '-17'=>'没有权限',
            '-19'=>'禁止同时使用多个接口地址',
            '-20'=>'相同内容重复提交',
            '-21'=>'Ip鉴权失败',
            '-22'=>'Ip鉴权失败',
            '-23'=>'缓存无此序列号信息',
            '-601'=>'序列号为空，参数错误',
            '-602'=>'序列号格式错误，参数错误',
            '-603'=>'密码为空，参数错误',
            '-604'=>'手机号码为空，参数错误',
            '-605'=>'内容为空，参数错误',
            '-606'=>'ext长度大于9,参数错误',
            '-607'=>'参数错误 扩展码非数字',
            '-608'=>'参数错误 定时时间非日期格式',
            '-609'=>'rrid长度大于18,参数错误',
            '-610'=>'参数错误 rrid非数字',
            '-611'=>'参数错误 内容编码不符合规范',
            '-623'=>'手机个数与内容个数不匹配',
            '-624'=>'扩展个数与手机个数不匹配',
            '-625'=>'定时时间个数与手机个数不匹配',
            '-626'=>'rrid个数与手机个数不匹配'
        ];
    
        $sn = 'SDK-BBX-010-23898';
        $password = '4-Fc1F-4';
        $data = MD5($sn.$password);
        $pwdMd5 = strtoupper($data);
        $url = 'http://sdk.entinfo.cn:8061/webservice.asmx/mdsmssend?sn='.$sn.
            '&pwd='.$pwdMd5.
            '&mobile=' . $mobile .
            '&content=' . $content .
            '&ext=&stime=&rrid=&msgfmt=';
        
        $html = file_get_contents($url, true);
        
        $p = xml_parser_create();
        xml_parse_into_struct($p, $html, $vals, $index);
        xml_parser_free($p);
        
        foreach ($arr as $k => $v){
            if($k == $vals[0]['value']){
                return  $v;
            }
        }
        
        return true;
    }
    
    // 绑定手机号
    function bindMobile($openid, $mobile, $code)
    {
        global $Db;
        
        $member = $Db->getOne("select * from ecm_member where openid = '$openid'");
        
        if (!$member) {
            showerr('openid error');
        }
        
        if (!empty($member['phone_mob'])) {
            showerr('mobile is binding');
        }
        
        $pwd = mt_rand(100000, 999999); // 随机密码
        
        // 更新手机号到用户账号
        $update = $Db->update(
            'ecm_member',
            array(
                'phone_mob' => $mobile,
                'password' => md5($pwd)
            ),
            "user_id = {$member['user_id']}"
        );
        
        $content = "【喜乐迷】您的登陆账号为手机号，登陆初始密码为" . $pwd . "。";
        
        $res = sentNodeMsg($mobile, $content);
        
        if (!$res) {
            showerr('note send fail');
        }
        
        showsuc('bind success');
    }

    
?>